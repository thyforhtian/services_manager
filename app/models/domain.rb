class Domain < ActiveRecord::Base
  attr_accessible :billed_price, :expiry_date, :name, :real_price, :renewal_date, :user_id

  belongs_to :user
end
