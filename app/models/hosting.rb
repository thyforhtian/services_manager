class Hosting < ActiveRecord::Base
  attr_accessible :expiry_date, :name, :renewal_date, :user_id, :billed_price

  belongs_to :user
end
