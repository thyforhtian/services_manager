class User < ActiveRecord::Base
  attr_accessible :email, :name, :nick

  has_many :domains
  has_many :hostings
end
