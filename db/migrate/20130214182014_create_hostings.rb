class CreateHostings < ActiveRecord::Migration
  def change
    create_table :hostings do |t|
      t.string :name
      t.date :renewal_date
      t.date :expiry_date

      t.references :user
      t.timestamps
    end
  end
end
