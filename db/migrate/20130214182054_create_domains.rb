class CreateDomains < ActiveRecord::Migration
  def change
    create_table :domains do |t|
      t.string :name
      t.date :renewal_date
      t.date :expiry_date
      t.float :real_price
      t.float :billed_price

      t.references :user
      t.timestamps
    end
  end
end
