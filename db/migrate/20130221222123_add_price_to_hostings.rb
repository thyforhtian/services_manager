class AddPriceToHostings < ActiveRecord::Migration
  def change
    add_column :hostings, :billed_price, :decimal
  end
end
