class ChangeColumnsInDomains < ActiveRecord::Migration
  def up
  	change_column :domains, :billed_price, :decimal
  	change_column :domains, :real_price, :decimal
  end

  def down
  	change_column :domains, :real_price, :float
  	change_column :domains, :billed_price, :float
  end
end
