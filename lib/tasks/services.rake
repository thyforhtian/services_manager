namespace :services do
  desc "checks for expiring services"
  task :check => :environment do
  	users = User.all
  	exp_domains = {}
  	exp_hostings = {}
  	
  	users.each do |user|
  		
  		user.domains.each do |domain|
  			if (domain.expiry_date - Date.current).to_i < 14
  				exp_domains[domain.name] = user.name
  			end
  		end

  		user.hostings.each do |hosting|
  			if (hosting.expiry_date - Date.current).to_i < 14
  				exp_hostings[hosting.name] = user.name
  			end
  		end
  	end

  	ServicesMailer.check_mail(exp_domains,exp_hostings).deliver
  end
end
